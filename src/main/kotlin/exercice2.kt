import java.lang.NumberFormatException

fun main() {
    convertToDouble("7.1")
    convertToDouble("9.")
    convertToDouble(".2")
    convertToDouble("tres")
}

private fun convertToDouble(num: String){
    try {
        println(num.toDouble())
    } catch (e: NumberFormatException){
        println("1.0")
    }
}