fun main() {
    println(divideixoCero(7, 2))
    println(divideixoCero(8, 4))
    println(divideixoCero(5, 0))
}

private fun divideixoCero(firstNum: Int, secondNum: Int): Int {
    var result = 0
    try {
        result = firstNum / secondNum
    } catch (e: ArithmeticException) {
        print("Error: No es pot divider per zero - ")
    }
    return result
}