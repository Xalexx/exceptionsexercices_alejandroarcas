import java.util.*

fun main() {
    val list = mutableListOf<Int>()
    val iterator = list.iterator()
    try {
        iterator.next()
    } catch (e: NoSuchElementException) {
        println("Se produjo una excepción: ${e.message}")
    }

    /*
    ------------------------------------------------------------------------------------------------------------------
    No se me ha ocurrido ningun ejemplo para la excepcion "UnsupportedOperationException", ya que con el siguiente
    ejemplo antes de que se lanze la excepcion salta el error: Unresolved reference add(), pero seria un buen ejemplo.
    ------------------------------------------------------------------------------------------------------------------
    val listOfNum = listOf<Int>()
    try {
        listOfNum.add(4)
    } catch (e: UnsupportedOperationException){
        println("Error: estas intentando modificar el size de una lista inmutable")
    } */
}
