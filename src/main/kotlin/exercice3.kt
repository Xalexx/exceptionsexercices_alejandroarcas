import java.io.File
import java.io.IOException

fun main() {
    readFile(File("./files/file.txt"))
}

fun readFile(file: File){
    try {
        val content = file.readText()
        println(content)
    } catch (e: IOException) {
        println("S'ha produït un error d'entrada/sortida: $e")
    }
}